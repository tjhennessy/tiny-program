# Description
  
This project is an exploration of program execution which will include a small bit  
of emphasis on compiling, linking, loading.  Furthermore, it will lead to some  
general unpacking of Linux binaries (ELF).  
  
## Contents
  
![Structure](images/structure.png?raw=true) 
  
## Usage
  
From within the content directory do:  
usage: ```./build```   
 
## Where Does it all _start?
  
### Overview
Where does program execution begin? The simple answer is ```main().```  
The better answer is ```_start```.  Neither are accurate enough to suffice.  
From a programmers perspective the start of a program is main.  From the perspective  
of program execution it is ```_start```, or an equivalent address where the  
loader goes to begin execution. 
  
During compilation the resulting binary is formatted as an Executable and
Linkable Formatted (ELF) file.  This introduces quite a bit of extra stuff
outside of the extra stuff added by the linker.  
      
### Linking, Loading, _Starting
  
During the linking stage of compilation, executable programs receive object  
code which is responsible for starting program execution.  This object code  
is added to the beginning of the program memory map.  The linker links in  
this object code from an relocatable file named ```crt*.o``` which contains   
the ```_start```  symbol, or an address to where program execution really  
begins.    
  
#### Usefully Informative Commands
   
Running the following command will display the overall ELF header information:
```
objdump -f <executable>
```
Example output:
```  
a.out:     file format elf32-i386
architecture: i386, flags 0x00000150:
HAS_SYMS, DYNAMIC, D_PAGED
start address 0x000003b0 <-- where we will look for _start
```
  
Running the following command will display the ```_start``` instructions:  
```
objdump --disassemble a.out
```
Example output:  
```
Disassembly of section .text:

000003b0 <_start>:
 3b0:	31 ed                	xor    %ebp,%ebp
 3b2:	5e                   	pop    %esi
 3b3:	89 e1                	mov    %esp,%ecx
 3b5:	83 e4 f0             	and    $0xfffffff0,%esp
 3b8:	50                   	push   %eax
 3b9:	54                   	push   %esp
 3ba:	52                   	push   %edx
 3bb:	e8 22 00 00 00       	call   3e2 <_start+0x32>
 3c0:	81 c3 1c 1c 00 00    	add    $0x1c1c,%ebx
 3c6:	8d 83 94 e5 ff ff    	lea    -0x1a6c(%ebx),%eax
 3cc:	50                   	push   %eax
 3cd:	8d 83 34 e5 ff ff    	lea    -0x1acc(%ebx),%eax
 3d3:	50                   	push   %eax
 3d4:	51                   	push   %ecx
 3d5:	56                   	push   %esi
 3d6:	ff b3 1c 00 00 00    	pushl  0x1c(%ebx)
 3dc:	e8 af ff ff ff       	call   390 <__libc_start_main@plt>
 3e1:	f4                   	hlt    
 3e2:	8b 1c 24             	mov    (%esp),%ebx
 3e5:	c3                   	ret 
```  
  
#### Linker Relocation
  
The linking phase combines user specified object files as well as ones provided by the system.  
  
![Linking Stage Graphic](images/linking.png?raw=true "Linking Graphic")
  
#### How the Shell Kicks Off a Program
  
When an executable is invokved, the shell calls ```execve```.  This system call will set up  
the stack such that argc, argv, and envp are on it.  After this point, the loader handles  
relocations and then calls ```_start```.  
  
![Shell execve()](images/shell_execve.png?raw=true "Shell execve")
  
#### Process Memory Image Complete, Time to _Start
  
The loader is now ready to start ```_start```.  The instructions in this section initializes  
the stack with the arguments required for calling the ```_libc_start_main()``` function.  
Below is the ```_libc_start_main()``` function declaration.   
  
#### Almost to Main, __libc_start_main -> main
  
```
int __libc_start_main(int (*main) (int, char **, char **),
                      int argc,
                      char **ubp_av,
                      void (*init) (void),
                      void (*fini) (void),
                      void (*rtld_fini) (void),
                      void (*stack_end)); 
```
  
This prototype has some awfully familiar elements.  The first argument is a function pointer  
to the ```main()```.  The next two arguments seem like the arguments we expect ```main()```  
to take.  By the names of the others we can deduce they must be responsible for initializing  
and finalizing something.  After these actions occur ```__libc_start_main()``` calls  
```main()```.   
  
Notes on ```__libc_start_main```:  
-Prepares environment variables used in program execution.  
-Starts up the program's threading.  
-Calls ```_init()``` function which does some initialization before main().  Think of the  
gcc compiler ```__attribute__ ((constructor))``` keyword which defines custom routines that  
run before the ```main()``` function is run.  
-Registers ```_fini()``` and ```_tfld_fini()``` functions which do cleanup after the  
program terminates.  The ```_fini()``` function is sort of the inverse of the ```_init()```  
function.  This is when the gcc ```__attribute__ ((destructor))``` keyword defined routines  
run.  
  
### Summary
Now is a good time to introduce the call graph which shows the general flow of execution:  
  
![Program Execution Call Graph](images/call_graph.png?raw=true "Call Graph")
  
Finally, the actual main function defined by the user is called.   
  
## Sources
http://dbp-consulting.com/tutorials/debugging/linuxProgramStartup.html  
https://embeddedartistry.com/blog/2019/4/8/a-general-overview-of-what-happens-before-main  
https://muppetlabs.com/~breadbox/software  
https://www.linuxquestions.org/questions/programming-9/_start-_init-and-frame_dummy-functions-810257/  
http://www.mentalcases.net/texts/security/TextSectionStudy.txt  
Learning Linux Binary Analysis - Ryan "elfmaster" O'Neill  
C and C++ Compiling - Milan Stevanovic  
